const initialState = {
    isLogin:false,
    token:null
};
export default (state =initialState ,action)=>{

      switch(action.type){
          case "LOGIN_SUCCESS":
              return {
                  ...state,
                  isLogin: true,
                  token: action.payload
              }
          case "LOGIN_FAILED":
              return{
                  ...state,
                  isLogin: false,
                  token:null
              }
          default:
              return state;
      }
}