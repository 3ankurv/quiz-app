import React,{useEffect} from "react";
import Question from "./question";
import Answers from "./answers";
import SubmitContainer from "./submitContainer";
import {connect} from "react-redux";
import { getQuizeQuestions } from "./action/action";

function QuizContainer({currentQuestionInfo,questionList,currentQuestionIndex,loadQuestionList}){
    console.log(currentQuestionInfo,"inside container==")

    useEffect(()=>{
      loadQuestionList();
    },[])

    return(
        <div className="quiz-container">
          {
            questionList.length ? 
            ( 
            <React.Fragment>
            <Question allQuestions={questionList} questionIndex={currentQuestionIndex} questionInfo={currentQuestionInfo}/>
            <Answers questionInfo={currentQuestionInfo} />
            <SubmitContainer/> </React.Fragment>) : "Preparing exam..."
          }
          
        </div>
    );

}

const mapStateToProps = (state)=>{
    return {
      questionList : state.quizReducer.questionsList,
      currentQuestionIndex: state.quizReducer.currentQuestionIndex,
      currentQuestionInfo: state.quizReducer.currentQuestionInfo
    }
  }

  function mapDispatchToProps(dispatch){
    return {
      loadQuestionList : ()=>dispatch(getQuizeQuestions())
    }
  }


export default connect(mapStateToProps,mapDispatchToProps)(QuizContainer) ;