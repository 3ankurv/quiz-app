import { takeEvery,put,call}  from "redux-saga/effects"
import { doLogin } from "../api"
import { loginSuccess, loginFail } from "../action/action";

export  function*  workLoginAction(loginReq){
    const  resLog =  yield call(doLogin,loginReq.payload);

     console.log(resLog) 

     if(resLog.statusCode ==401){
         console.log(resLog)
         yield put(loginFail(resLog));
     }
     else{
         localStorage.setItem("accessToken",resLog.accessToken);
        yield put(loginSuccess(resLog));
     }

}