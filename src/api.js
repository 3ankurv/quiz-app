export const API_END_POINT = "http://localhost:3000/";
export const getQuizQuestionsReq = async () =>{
   return  await (await window.fetch(`${API_END_POINT}quiz`,{headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': getToken(),
    }})).json()
}




export const doLogin = async (req) =>{
   return await( await window.fetch(`${API_END_POINT}auth/signin-exam`,{
      method: 'post',
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json'
       },
      body: JSON.stringify(req)
    })).json()
}

export const isActive = async () =>{
   return  await (await window.fetch(`${API_END_POINT}quiz/isactive`, { headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': getToken(),
    }}))

}


function getToken(){
   return `Bearer ${localStorage.getItem("accessToken")}`
}