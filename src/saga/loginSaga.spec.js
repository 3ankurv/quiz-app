import {testSaga} from "redux-saga-test-plan";
import {workLoginAction} from "./loginSaga";
import { doLogin } from "../api"
import { loginSuccess } from "../action/action";

describe("Login saga",()=>{

    let saga;

    it("should work correctly with actions",()=>{
        const param =  {payload: {uname:'test',password:'test'}} ;
        const validResponse ={statusCode:200, accessToken:'test'};
        saga = testSaga(workLoginAction,param);
        saga.next().call(doLogin,param.payload);
        saga.next(validResponse).put(loginSuccess(validResponse));
        saga.next().isDone()
    })

});