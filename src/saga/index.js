import {takeLatest,all} from "redux-saga/effects";
import {quizActionTypes} from "../constant/index";
import { quizRequestSaga,quizSuccess, quizeError } from "./quizSaga";
import { workLoginAction } from "./loginSaga";
export default function* rootSaga(){
    yield all([
        takeLatest(quizActionTypes.GET_QUESTIONS_REQUEST,quizRequestSaga),
        takeLatest(quizActionTypes.GET_QUESTIONS_SUCCESS,quizSuccess),
        takeLatest(quizActionTypes.GET_QUESTIONS_SUCCESS,quizeError),
        takeLatest("LOGIN_REQUEST",workLoginAction)
     
    ])
}