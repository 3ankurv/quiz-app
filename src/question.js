import React from "react";
import TimeCountDown from "./TimeCountDown";

function Question({questionInfo,questionIndex,allQuestions}){
    console.log(questionInfo,"inside question");
    return(
        <div className="question">
            <h4> {`Question ${questionIndex+1}/${allQuestions.length}`}</h4>
           <TimeCountDown/>
    <p className="question-text">{questionInfo && questionInfo.title || '' }</p>
    <p style={{color:"#FFF",textAlign:"left",padding:"5px"}}>{questionInfo && questionInfo.description}</p>
        </div>
    );
}


export default Question;