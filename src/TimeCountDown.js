import React,{useState,useEffect} from "react";

function TimeCountDown(){

    let currentDate = new Date();
    
    currentDate.setHours(currentDate.getHours()+1);
   
    const deadLine = "Dec, 16, 2020";

    const [timeCountDown,setTimeCountDown] = useState({day:0,h:0,m:0,s:0});
    const updateCountDown= (endDate)=>{
  
     const time = Date.parse(currentDate)-Date.parse(new Date());
     if(time>0){
          let s = Math.floor( (time/1000)%60)
          let m = Math.floor( (time/(1000*60) )%60)
          let h = Math.floor( (time/(1000*60*60))%24)
          let day = Math.floor( (time/(1000*60*60*24)))
          setTimeCountDown({day,h,m,s})
     }
  
    }
  
    useEffect(()=>{
          setInterval(()=>{
              updateCountDown(currentDate);
          },1000)
    },[])
    const {s,m} = timeCountDown;
    return(
    <div className="timer">{`${m}:${s}`}</div>
    )
}
export default TimeCountDown;