import LoginReducer from "./loginReducer";

describe("Login Reducer",()=>{
    
    let initialState ;
    beforeEach(()=>{
        initialState = {
            isLogin:false,
            token:null
        };
    });

    it('should return default state in unknown actions',()=>{
        expect(LoginReducer(undefined,{type:undefined})).toEqual(initialState);
    });

    it('should return correct state on login success action',()=>{

        const action = {type:"LOGIN_SUCCESS",payload:"test-token"};
        const expectedResult = {
            ...initialState,
            isLogin:true,
            token:'test-token'
        };
        expect(LoginReducer(initialState,action)).toEqual(expectedResult);
    })

    it("should return correct state on login faied action",()=>{
        const action = {type:"LOGIN_FAILED"};
        expect(LoginReducer(initialState,action)).toEqual(initialState);
    
    })
})