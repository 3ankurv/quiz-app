import {combineReducers}  from "redux";
import quizReducer from "./quizReducer";
import loginReducer from "./loginReducer";

export default combineReducers({
    quizReducer,
    loginReducer
})
