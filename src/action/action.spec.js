import configureStore from "redux-mock-store";
import { quizActionTypes } from "../constant";

import {
    getQuizeQuestions,
    quizQuesttionSuccess,
    quizQuesttionFail,
    setCurrentQuestionIndex,
    setCurrentQuestionInfo,
    doLoginRequest,
    loginSuccess,
    loginFail
} from "./action";

const { GET_QUESTIONS_SUCCESS, GET_QUESTIONS_REQUEST, GET_QUESTIONS_ERROR,
    SET_CURRENT_QUESTION_INDEX,SET_CURRENT_QUESTION_INFO
} = quizActionTypes;


const mockStore = configureStore();
const store = mockStore();

describe("Redux actions", () => {

    beforeEach(() => {
        store.clearActions();
    })

    it(`should dispatch the ${GET_QUESTIONS_REQUEST}`, () => {
        const expectActions = [{ type: GET_QUESTIONS_REQUEST }];
        store.dispatch(getQuizeQuestions())
        expect(store.getActions()).toEqual(expectActions);
    });

    it(`should dispatch the ${GET_QUESTIONS_SUCCESS}`, () => {
        const questionsList = [{ title: "test question" }]
        const expectActions = [{ type: GET_QUESTIONS_SUCCESS, payload: questionsList }];
        store.dispatch(quizQuesttionSuccess(questionsList))
        expect(store.getActions()).toEqual(expectActions);
    });

    it(`should dispatch the ${GET_QUESTIONS_ERROR}`, () => {
        const expectActions = [{ type: GET_QUESTIONS_ERROR, payload: { msg: "Something went wrong" } }];
        store.dispatch(quizQuesttionFail())
        expect(store.getActions()).toEqual(expectActions);
    });

    it(`should dispatch the ${SET_CURRENT_QUESTION_INDEX}`, () => {
        const questionIdx = 1;
        const expectActions = [{ type: SET_CURRENT_QUESTION_INDEX, payload: questionIdx }];
        store.dispatch(setCurrentQuestionIndex(questionIdx))
        expect(store.getActions()).toEqual(expectActions);
    });


    it(`should dispatch the ${SET_CURRENT_QUESTION_INFO}`, () => {
        const questionInfo = {"title":"test question"};
        const expectActions = [{ type: SET_CURRENT_QUESTION_INFO, payload: questionInfo}];
        store.dispatch(setCurrentQuestionInfo(questionInfo))
        expect(store.getActions()).toEqual(expectActions);
    });

    it(`should dispatch the LOGIN_REQUEST`, () => {
        const reqParam = {"name":"test","password":'test'};
        const expectActions = [{ type: 'LOGIN_REQUEST', payload: reqParam}];
        store.dispatch(doLoginRequest(reqParam))
        expect(store.getActions()).toEqual(expectActions);
    });

    it(`should dispatch the LOGIN_SUCCESS`, () => {
        const reqParam = {"token":"test-token"};
        const expectActions = [{ type: 'LOGIN_SUCCESS', payload: reqParam}];
        store.dispatch(loginSuccess(reqParam))
        expect(store.getActions()).toEqual(expectActions);
    });


    it(`should dispatch the LOGIN_FAILED`, () => {
        const error ={msg:"Invalid Credential"};
        const expectActions = [{ type: 'LOGIN_FAILED', payload: error}];
        store.dispatch(loginFail())
        expect(store.getActions()).toEqual(expectActions);
    })
})
