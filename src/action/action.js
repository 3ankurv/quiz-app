import { quizActionTypes }  from "../constant";

export const getQuizeQuestions = ()=>{
    return {
        type:quizActionTypes.GET_QUESTIONS_REQUEST
    }
}

export const quizQuesttionSuccess = (data) =>{
    return {
        type: quizActionTypes.GET_QUESTIONS_SUCCESS,
        payload:data
    }
}

export const quizQuesttionFail = () =>{
    return {
        type: quizActionTypes.GET_QUESTIONS_ERROR,
        payload:{msg:"Something went wrong"}
    }
}
 
export const setCurrentQuestionIndex = (idx)=>{
    return {
        type: quizActionTypes.SET_CURRENT_QUESTION_INDEX,
        payload:idx
    }
}

export const setCurrentQuestionInfo = (info)=>{
    return{
        type: quizActionTypes.SET_CURRENT_QUESTION_INFO,
        payload: info
    }
}

export const doLoginRequest = (reqObj)=>{
    return {
        type:"LOGIN_REQUEST",
        payload:reqObj
    }
}

export const loginSuccess = (res)=>{
     return {
         type:"LOGIN_SUCCESS",
         payload:res
     }
}

export const loginFail = ()=>{
    return {
        type:"LOGIN_FAILED",
         payload:{msg:"Invalid Credential"}
    }
}