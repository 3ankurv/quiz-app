import React, { useEffect, useState } from 'react';
import './App.css';
import QuizContainer from './quizContainer';
import {connect} from "react-redux";
import {getQuizeQuestions} from "./action/action";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import ValidatedLoginForm from './LoginForm';
import { isActive } from './api';

function App({questionList,loadQuestionList,loginDetail}) {
console.log(loginDetail);
const [isActiveUser,setIsActiveUser] = useState(false);
const [isLoading, setLoading] = useState(true);
  useEffect( async ()=>{
    console.log("its loading ====>");
     const resUser =  await isActive();
     console.log(resUser);
     setLoading(false);
     if(resUser.status===200){
        setIsActiveUser(true)
     }else{
      setIsActiveUser(false)
    }
  },[])

  return (
    <div className="App">

      <Router>
        <Switch>
        <Route exact path="/">
     

      {
         !isLoading ? (isActiveUser ?  <QuizContainer /> :     <ValidatedLoginForm /> ) : "Loading.." 
      }
        </Route>
        {/* <Route path="/exam">
          <QuizContainer />
        </Route> */}
      </Switch>
      </Router>
    
    </div>
  );
}


function mapStateToProps(state){

  return{
    loginDetail: state.loginReducer
  }
}
function mapDispatchToProps(dispatch){
  return {
    loadQuestionList : ()=>dispatch(getQuizeQuestions())
  }
}




export default connect(mapStateToProps,mapDispatchToProps)(App);
