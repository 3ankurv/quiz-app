import React from "react";
import { connect } from "react-redux";
import {setCurrentQuestionIndex} from "./action/action";
function SubmitContainer({prevQues,nextQues, currentQuestionIndex,questionList }){

    console.log(currentQuestionIndex)
    return (
        <div className="prev-next-cont">
          {currentQuestionIndex ? <button onClick={()=>prevQues(currentQuestionIndex)} className="button">Previous</button> : null } 
            <button disabled={currentQuestionIndex+1===questionList.length}  onClick={()=>nextQues(currentQuestionIndex)} className="button">Next</button>
            <button className="button">Submit</button>
        </div>
    );
}

const mapStateToProps = (state)=>{
    return {
      questionList : state.quizReducer.questionsList,
      currentQuestionIndex: state.quizReducer.currentQuestionIndex,
      currentQuestionInfo: state.quizReducer.currentQuestionInfo
    }
  }


const mapDispatchToProps = (dispatch)=>{
  return {
    prevQues:(currIdx)=>dispatch( setCurrentQuestionIndex(currIdx-1) ),
    nextQues:(currIdx)=>dispatch( setCurrentQuestionIndex(currIdx+1) )
  }
}  

export default connect(mapStateToProps,mapDispatchToProps)(SubmitContainer);