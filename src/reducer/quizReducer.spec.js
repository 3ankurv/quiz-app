import QuizReducer from "./quizReducer";

describe("Quiz Reducer",()=>{
let initialState;

beforeEach(()=>{
    initialState = {
        questionsList : [],
        currentQuestionIndex:0,
        currentQuestionInfo:null,
        error:""
    }
})

it('should return default state on unknown action',()=>{

    expect(QuizReducer(undefined,{type:undefined})).toEqual(initialState)
})

})