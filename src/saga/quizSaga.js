import {call,put} from "redux-saga/effects";
import {getQuizQuestionsReq} from "../api";
import {quizActionTypes} from "../constant/index";

export function* quizRequestSaga(){
    try{
        const reqQ = yield call(getQuizQuestionsReq)
    console.log(reqQ,"===?");
       // yield call(quizSuccess,reqQ);
       yield put({type: quizActionTypes.GET_QUESTIONS_SUCCESS,payload:reqQ });
       if(reqQ.length){
        yield put({type: quizActionTypes.SET_CURRENT_QUESTION_INDEX,payload:0});
        yield put({type: quizActionTypes.SET_CURRENT_QUESTION_INFO,payload:reqQ[0]});
       }
       
    }catch(e){
        //yield call(quizeError,e);
    }
    
}


export function* quizSuccess(data){

    
}

export function* quizeError(data){
  //  yield put({type: quizActionTypes.GET_QUESTIONS_ERROR,payload:data });
}

export function* setQuestionIndex(idx){
    yield put({type: quizActionTypes.SET_CURRENT_QUESTION_INDEX,payload:idx});
}
