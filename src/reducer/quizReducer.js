import {quizActionTypes} from "../constant";
const initialState = {
    questionsList : [],
    currentQuestionIndex:0,
    currentQuestionInfo:null,
    error:""
}
export default (state=initialState,action)=>{

    switch(action.type){
        case quizActionTypes.GET_QUESTIONS_SUCCESS:
            return {
                ...state,
                 questionsList : action.payload
            }
        case quizActionTypes.GET_QUESTIONS_ERROR:
            return{
                ...state,
                error: action.payload
            }
            case quizActionTypes.SET_CURRENT_QUESTION_INDEX:
                return{
                    ...state,
                    currentQuestionIndex: action.payload,
                    currentQuestionInfo:state.questionsList[action.payload]
                }
            default:
                return state;
    }
    
  
}